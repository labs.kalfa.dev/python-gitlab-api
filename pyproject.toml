[tool.poetry]
name = "python-gitlab-api"
version = "0.1.3"
description = "Programmatic REST Client for GitLab"
authors = ["Cosimo Alfarano <cosimo@alfarano.net>"]
license = "LGPL-3.0-or-later"

readme = "README.md"
repository = "https://gitlab.com/labs.kalfa.dev/python-gitlab-api/"
homepage = "https://gitlab.com/labs.kalfa.dev/python-gitlab-api/"

packages = [
  { include = "gitlab"  },
  { include = "gitlab_cli" }
]

[tool.poetry.dependencies]
python = "^3.7"
requests = "^2.25.1"
PyYAML = "^5.4.1"
dataclasses = { version = "^0.8", python = ">=3.6,<3.7" }
url-normalize = "^1.4.3"
jsonpath-ng = "^1.5.2"
Jinja2 = "^2.11.3"
click = "^7.1.2"
packaging = "^20.9"

[tool.poetry.dev-dependencies]
pytest = "^6.2.2"
pytest-cov = "^2.11.1"
mypy = "^0.812"
PyHamcrest = "^2.0.2"
pylint = "^2.7.2"
py2puml = "^0.4.0"

types-dataclasses = "^0.6.1"

[tool.poetry.scripts]
publish-release = 'gitlab_cli.ci_utils:publish_release'
get-package-version = 'gitlab_cli.ci_utils:get_package_version'
get-latest-package-version = 'gitlab_cli.ci_utils:get_latest_release_version'
get-releases = 'gitlab_cli.get:Releases'
get-release = 'gitlab_cli.get:Release'
get-user = 'gitlab_cli.get:User'
get-project = 'gitlab_cli.get:Project'
get-projects = 'gitlab_cli.get:Projects'
get-groups= 'gitlab_cli.get:Groups'
get-group= 'gitlab_cli.get:Group'
get-group-wikis = 'gitlab_cli.get:GroupWikis'
get-group-wiki = 'gitlab_cli.get:GroupWiki'
get-project-wikis = 'gitlab_cli.get:GroupWikis'
get-project-wiki = 'gitlab_cli.get:GroupWiki'
create-release = 'gitlab_cli.create:Release'
create-group-wikis = 'gitlab_cli.create:GroupWikiPage'
create-project-wikis = 'gitlab_cli.create:ProjectWikiPage'

[build-system]
requires = ["poetry-core>=1.1.0"]
build-backend = "poetry.core.masonry.api"

[tool.pytest.ini_options]
markers = [
    "end2end: end to end tests which require Internet connection and will change live REST entities"
]

[tool.isort]
profile = "black"
