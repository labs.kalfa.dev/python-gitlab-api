"""Tests"""
from unittest.mock import Mock

from gitlab.crud import APIRead
from gitlab.crud import APICreate

# pylint: disable=missing-function-docstring,missing-docstring
# pylint: disable=too-few-public-methods

# allow fixtures to work in pytest:
# pylint: disable=redefined-outer-name


# Communication between the endpoint interface and the API* CRUD classes  which consumes it

# APIRead and endpoint.read
class SomeRESTReadAPI(APIRead):
    path = "/fake/path"


def test_data_read_by_endpoint_is_in_status(stub_endpoint):
    stub_endpoint.set_read({
            'attribute': Mock()
        })

    api = SomeRESTReadAPI(stub_endpoint)
    api.read()

    assert 'attribute' in api.status


def test_data_not_read_by_endpoint_is_not_in_status(stub_endpoint):
    stub_endpoint.set_read({})

    api = SomeRESTReadAPI(stub_endpoint)
    api.read()

    assert 'i_dont_exist' not in api.status



# APICreate and  endpoint.create
class SomeRESTCreateAPI(APICreate):
    path = "/fake/path"


def test_data_created_by_endpoint_is_in_status(stub_endpoint):
    stub_endpoint.set_create({
            'attribute': Mock()
        })

    api = SomeRESTCreateAPI(stub_endpoint)
    api.create()

    assert 'attribute' in api.status


def test_data_not_created_by_endpoint_is_not_in_status(stub_endpoint):
    stub_endpoint.set_read({})

    api = SomeRESTCreateAPI(stub_endpoint)
    api.create()

    assert 'i_dont_exist' not in api.status
