"""Component tests depending on internet access
"""
import os

import pytest
from gitlab import endpoints, resources
from hamcrest import assert_that, calling, has_entry, has_key, not_, raises

# pylint: disable=missing-function-docstring,missing-docstring
# pylint: disable=too-few-public-methods


def setup_module(unused_module):
    endpoints.enable_http_transactions_debug()


def _test_env_is_not(name: str):
    return os.environ.get("TEST_ENVIRONMENT", None) != name


@pytest.mark.skipif(
    _test_env_is_not("gitlab_com"), reason="gitlab.com API data requiered for test"
)
def test_can_obtain_user(gitlab_endpoint, gitlab_user_id):
    user = resources.User(gitlab_endpoint, user_id=gitlab_user_id)
    user.read()

    assert_that(user.status, not_(has_key("error")))
    assert_that(user.status, has_entry("id", gitlab_user_id))


@pytest.mark.skipif(
    _test_env_is_not("gitlab_com"), reason="gitlab.com API data requiered for test"
)
def test_can_filter_users_against_username(gitlab_endpoint, gitlab_user_login):
    # get user id from username
    user_filter = dict(username=gitlab_user_login)

    users = resources.Users(gitlab_endpoint)
    users.read(attributes=user_filter)

    assert "error" not in users.status
    assert len(users.status) >= 1
    assert users.status[0]["username"] == gitlab_user_login


@pytest.mark.skipif(
    _test_env_is_not("gitlab_com"),
    reason="gitlab custom API data requiered for test",
)
def test_can_modify_an_existing_user(gitlab_endpoint, gitlab_user_id):
    user = resources.User(gitlab_endpoint, gitlab_user_id)
    user.read()

    current_skype_id = user.status["skype"]
    # change it to the same value, so that if for any reason it succeeds, it will change nothing
    # (although this operation is expceted to return 403)
    update_skype = dict(skype=current_skype_id)

    assert_that(user.status, not_(has_key("error")))
    # only administrator can change user data, but the fact that it says Forbidden it means that it performed the
    # UPDATE request against the user
    assert_that(
        calling(user.update).with_args(attributes=update_skype),
        raises(endpoints.HTTPRequestNotOK),
    )


@pytest.mark.skipif(
    _test_env_is_not("gitlab_com"), reason="gitlab.com API data requiered for test"
)
def test_can_modify_an_existing_project(gitlab_endpoint):
    project = resources.Project(gitlab_endpoint, "24849676")
    project.read()

    # double check the project is what is supposed to be
    assert_that(project.status, has_entry("name", "gitlab_api_for_tests"))

    current_visibility = project.status["visibility"]
    expected_visibility = "private" if current_visibility == "public" else "public"

    project.update(attributes={"visibility": expected_visibility})

    assert_that(project.status, has_entry("visibility", expected_visibility))
