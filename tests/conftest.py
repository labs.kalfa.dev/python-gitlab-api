"""Fixtures
"""
import os

from pytest import fixture

from gitlab.typing import JSON
from gitlab import endpoints
from gitlab import resources

# pylint: disable=missing-function-docstring,missing-docstring
# pylint: disable=too-few-public-methods

# allow fixtures to work in pytest:
# pylint: disable=redefined-outer-name


@fixture
def gitlab_auth_token():
    return os.environ['API_AUTH_TOKEN']



@fixture
def gitlab_url():
    return os.environ['CI_API_V4_URL']


@fixture
def gitlab_endpoint(gitlab_url, gitlab_auth_token) -> endpoints.IEndpoint:
    """Gitlab endpoint for public information

    No authorization required"""
    return endpoints.GitLabEndpoint(gitlab_url, token=gitlab_auth_token)


class StubEndpoint(endpoints.GitLabEndpoint):
    """A fake IEndpoint which allows setting CRUD answers
    """
    def __init__(self):  # pylint: disable=super-init-not-called
        self.__read_return = {}
        self.__create_return = {}

    # extra methods to set results on fake
    def set_read(self, data: dict):
        self.__read_return = data

    def set_create(self, data: dict):
        self.__create_return = data
    # end of extra methods

    # pylint: disable=unused-argument
    def create(self, path: str, data: JSON):
        return self.__create_return

    def read(self, path: str) -> JSON:
        return self.__read_return
    # pylint: enable=unused-argument



@fixture
def stub_endpoint() -> endpoints.GitLabEndpoint:
    """Fake endpoint to test programmatic behaviours in isolation
    """
    return StubEndpoint()


@fixture
def gitlab_user_login() -> str:
    return os.environ['GITLAB_USER_LOGIN']


@fixture
def gitlab_email() -> str:
    return os.environ['GITLAB_USER_EMAIL']


@fixture
def gitlab_project_id(gitlab_endpoint, gitlab_user_login) -> str:
    return os.environ['CI_PROJECT_ID']


@fixture
def gitlab_user_id() -> int:
    return int(os.environ['GITLAB_USER_ID'])
