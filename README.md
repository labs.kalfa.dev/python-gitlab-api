# python-gitlab-api: GitLab API

This pacakge provides two things:
- a simple python class based implementation of the GitLab v4 API.
- command-line utilities to query the API via shell

See the [ARCHITECTURE file](ARCHITECTURE.md)


Starting using the module's classes is pretty easy:
- specify the endpoint, which is URL + authorization token (or anon access)
- use a resouce among the ones provided in gitlab.resource
- use a CRUD operation, among the ones supported for the resource
- pass paramters to the operation to filter the results


In the following case it is used a predefined GitLab.com endpoint, with no token paramter passed (anonymous).
The Projects resource used with a Read operation  will list some/all projects.
```
from gitlab.utils import ppjson
from gitlab.resources import Projects
from gitlab.endpoints import GitLabDotCom 

ppjson(Projects(GitLabDotCom()).read())
```

In following case it is used a predefined GitLab.com endpoint, with a token.
The Projects resource used with a Read operation  will list some/all projects.
```
from gitlab.utils import ppjson
from gitlab.resources import Projects
from gitlab.endpoints import GitLabDotCom 

ppjson(Projects(GitLabDotCom(token='SOME TOKEN')).read({'owned':True}))
```

In the following case, a generic endpoint is used. `GitLabEndpoint' can receive in its constructor an URL and token, or take it from the environment.
The environmental variables `API\_AUTH\_TOKEN` and `GITLAB_API_V4_URL` (or `CI_API_V4_URL` to be compatible with GitlabCI environments) should be set.
e.g.
```
API_AUTH_TOKEN='123456'
CI_API_V4_URL='https://gitlab.aws.agileanalog.com/api/v4'
```
And the related code using such environment:
```
from gitlab.utils import ppjson
from gitlab.resources import Projects
from gitlab.endpoints import GitLabEndpoint

ppjson(Projects(GitLabEndpoint.from_env()).read({'owned':True}))
```

## Tokens

Currently only [personal access](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) tokens are supported.

Feel free to suggest ways to introduce the support for OAuth2 contacting the author and then proposing a change with a merge request.

## Utilities

Some command-line utilties which maps 1:1 to the API resources are provided as entry-points of the python package.

The utlities are meant to be used for quick scripting, but they support environmental variables and `dash-dash` parameters, so that they can be used in CICD environment.

Such mapping utilities are meant to return JSON, as provided by the API.

### Package versions

Some basic version management can be used in CICD to automatically release or query about the release version of a package

- get-package-version and get-latest-package-version query the Release resource, and provide either tha latest release version or bump the next patch version based on the latest release version recorded in gitlab
- get-



## Contributions

All contribution are welcome, in the best open source tradition.

Before adding missing features, please contact the author first to discuss best ways to do it.

New resources can be added via merge requests directly, as they are supposed to be really easy to add.
