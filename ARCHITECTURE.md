# Design

```plantuml
@startuml

package "GitLab API Endpoints" <<cloud>>  {

entity   "/api/v4/" as Endpoint

entity   "/api/v4/users" as Endpoint_Users
entity   "/api/v4/groups" as Endpoint_Groups
entity   "/api/v4/runners" as Endpoint_Runners
entity   "/api/v4/projects" as Endpoint_Projects
}


together {
interface          gitlab.crud.IAPICreate
interface          gitlab.crud.IAPIRead
interface          gitlab.crud.IAPIUpdate
interface          gitlab.crud.IAPIDelete

' note as gitlab.crud.crud_note
' An Create/Rread/Update/Delete operation whic instruct the endpoint what to do in order to be performed against the remote API
' end note
}

together {

interface          gitlab.endpoints.IEndpoint

' note as gitlab.endpoints.endpoints_note
' An endpoint manages the HTTP connection toward the REST API endpoint URL, abstracting the CRUD operatoins performed against it
' end note
}

together {
class gitlab.resources.Users
class gitlab.resources.Groups
class gitlab.resources.Runners
class gitlab.resources.Projects
class gitlab.resources.WhateverIsNeeded


' note as gitlab.resources.resources_note
' A resource uses the basic Create/Read/Update/Delete operation, to communicate with the endpoint.
' Each resrouce can implement one or more CRUD operation, depending on its semantics
' end note

}

gitlab.endpoints.IEndpoint --* gitlab.resources.Users
gitlab.endpoints.IEndpoint --* gitlab.resources.Groups
gitlab.endpoints.IEndpoint --* gitlab.resources.Runners
gitlab.endpoints.IEndpoint --* gitlab.resources.Projects

gitlab.endpoints.GitlabDotCom ..|> gitlab.endpoints.IEndpoint
gitlab.endpoints.GitlabDotCom -> Endpoint


gitlab.resources.Users .up. Endpoint_Users
gitlab.resources.Groups .up. Endpoint_Groups
gitlab.resources.Projects .up. Endpoint_Projects
gitlab.resources.Runners .up. Endpoint_Runners

gitlab.resources.Users .down.|> gitlab.crud.IAPIRead
gitlab.resources.Users .down.|> gitlab.crud.IAPIUpdate

gitlab.resources.Groups .down.|> gitlab.crud.IAPIRead

gitlab.resources.Projects .down.|> gitlab.crud.IAPIRead

gitlab.resources.Runners .down.|> gitlab.crud.IAPIRead

@end
```

[PlantUML diagram of the gitlab python package in master](https://labs.kalfa.dev.gitlab.io/python-gitlab-api/gitlab-master.png)

## REST Resources (gitlab.resources)

In `gitlab.resources` are present classes which represent each resource on https://docs.gitlab.com/ee/api/api_resources.html.

Since the classes are modelled as 1:1 representation against each resource in the API documentation, a user need to be familiar with the API itself.

Specifically, to use the API, it is required to understand what parameters can be used and what parameters are required by each resource and for each CRUD operation.

The required parameters to identify the resource, e.g. a `project-id` in order to identify a Project object in the REST API, is part of the `gitlab.resources.Project` class constructor.
Any other parameter that is used to filter (non-mandatory parameter), and not to identify a resrouce, is passed to the operation instead, e.g. 
```python
# Users is a Resource endpoint which represents all users in the system. It is possible to pass a specific username as filter (not to be confused with /users/:id which is a Single User resource endpoint
Users().read({'username': 'YOUR_LOGIN_USER'})
```


### Extending Resources

An resource is a class.

Resources that do not have required parameters are simple class which inherit from one of the CRUD classes.

```
class Resources(APIRead, APICreate):
   path = '/resources'
```

The above Resource implements READ and CREATE.
The operations are implemented by the parent classes, so there is nothing to do code wise.

**Note:** the current approach to resource implementation is new Resources are added as they are used and any implemented resource will have only the CRUD operation enabled as they are used. Any user can request or even better send a MR for adding or extending a resource.

The class needs to provide what is the path of the resrouce, via the `path` attribute.

This attribute will be passed to `gitlab.crud` during the execution of any CRUD operation, to inform it about what is the remote endpoint to communicated with.

### Mandatory parameters


Some resources have some mandatory parameter to identify it within the system. Those mandatory paramters will be part of the resource constructor.
By default a resource has only a IEndpoint passed, since at its basic it just requires to communicate via HTTP with the remove endpoint.

More complex resources needs to be indentified providing `project-id`s or `group-id`s. e.g.,  a resource, which is identified behind the `/resources` path with an id, like `/resources/:id`, is represented in python as:

```python
  class MyResource(APIRead, APIUpdate):
      def __init__(self, gitlab.endpoints.IEndpoint: IIEndpoint,
              resource_id: int) -> None:
          super().__init__(gitlab.endpoints.IEndpoint)

          self.path = f"/resources/{resource_id}"
```

Note that the name of the class is singular, even if it uses the `/resources` (plural) path, since identifies a single element (the specific resource having `resource_id` as _id_).


Similarly, a resouce with mandatory parameters will be instantiated this way.

```python
from gitlab.resources import Project
from gitlab.endpoint import GitLabDotCom

token = 'get it from GitLab.com'
endpoint = GitLabDocCom(TOKEN)
project = Project(endpoint, project_id)
```

### Filtering parameters

Some paramters are optional and required to filter within a bigger set of results.

Those paramters are not part of the constructor, sicne they do not indentify the resource.

They are instead passed to each CRUD operation whic require them.

Thereof, there is nothing to do when implementing a resource, to enable the filtering behaviour. It just works out of the box, since it's delegated to the CRUD operations and it's transformed in JSON internally.

From the end-user point of view, each parameter against which a result can be filtered, are passed as a mapping argument to the CRUD operation.
```python
from gitlab.resources import Users

user = Users().read({'username': 'kalfa'})
```

The read method is inherited by the `IAPIRead` interface implementation, by default `APIRead`. The operation is responsible to transform the dictionary in right format depending on how the backend works. By default of course, they are JSON and HTTP.

### Result of the CRUD operation

The result of a CRUD operation is returned by the operation method itself.

The **last result** - that's it the result of the last operation performed against a Resource instance - is stored in the status attribute of the instance itself.

**Note:** since a resource instance can be used to perform several different CRUD operations, one after the other, it is important to distinguish that the status attribute refers to the latest operation
```
from gitlab.resources import Users

users = Users(endpoint)

kalfa = users.read({'username': 'kalfa'})
assert users.status == kalfa

other = users.read({'username': 'other'})
assert users.status == others

# and obviously it is now different from the result of the first read operation
assert users.status != kalfa
```

## EndPoints (gitlab.endpoints)

The `gitlab.com` endpoint is present by default, and allows to connect to `gitlab.com` API endpoints.
For self managed installation, it is possible to create an endpoint programmatically with

```python
from gitlab.endpoints import GitLabEndpoint, GitLabDotCom

TOKEN = "your auth token"

# specify the URL of the self-managed instance endpoint
self_managed = GitLabEndpoint("https://my.own.end.po.int/api/v4", token=TOKEN)
# or just pass the token if it is not a self-managed instance
gitlab_com = GitLabDotCom(TOKEN)
```

The endpoint is passed to each `gitlab.resoruces` resource.

```plantuml
entity EndPoint
entity Resource

Resource *-- EndPoint
```

The token is obtained via the GitLab [user profile](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) interface.

## Crud Operations (gitlab.crud)

**Note:** This section is not relevant for any end user who wants to use the package. It's here to explain how the role of an operation within the gitlab API python package design.

4 [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) operation interfaces are provided and should be just used.
- IAPICreate, which implements the create() method
- IAPIRead, which implements the read() method
- IAPIUpdate, which implements the update() method
- IAPIDelete, which implements the delete() method

GitLab v4 API majority of endpoints are based on such operation and use HTTP verbs to map to CRUD verbs quite easily (few exceptions are present, though).

A CRUD operation has the responsibility to abstract the work a resource needs to do, so that a resource only needs to use the operation.
Each operation is meant to be used by all the resrouces requiring it. 

:warning: at the moment there are a limited number of resources implemented and actually not all the operations are used and thereof some might not be implemented yet. Open a MR with an operation implementation, the amend to the resoruce where it is needed and related tests, to add it to the releases

```plantuml
entity Resource
entity CRUD_Operation

CRUD_Operation "1..4" <|-- "0..N" Resource
```

## Contribute adding new Resources

To add a new resource it is enough to create a merge request with the new resources, adding proper testing of the MR.
